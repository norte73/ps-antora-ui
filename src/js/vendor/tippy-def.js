/* eslint-disable */
tippy.setDefaults({
  placement: 'top',
  arrow: true,
  arrowType: 'sharp',
  allowHTML: true,
  delay: [700, 0],
  /* delay: 400, */
  theme: 'light',
  followCursor: true,
  interactive: true,
});

tippy('.deprecated', {
  arrow: true,
  content: '<b>Deprecated</b><br/>' +
  'This feature/function has been removed<br>' +
  'and is not used in this release.',
  /* allowHTML: true,
  followCursor: true,
  interactive: true,
  theme: 'light', */
})

tippy('.preview', {
  arrow: true,
  content: '<b>Preview</b><br/>' +
  'We are trying out this new feature/function.<br/>' +
  'We would appreciate your feedback:<br/>' +
  '<a href="mailto:feedback@secmaker.com">feedback@secmaker.com</a>',
  /* allowHTML: true,
  followCursor: true,
  interactive: true,
  theme: 'light', */
})

tippy('.tobedeleted', {
  arrow: true,
  content: '<b>To be deleted</b><br/>' +
  'This page or section is to be removed<br>' +
  'in the next release or build.',
  /* allowHTML: true,
  followCursor: true,
  interactive: true,
  theme: 'light', */
})

tippy('.new', {
  arrow: true,
  content: '<b>New</b><br/>' +
  'This page or section is new or updated.',
  /* allowHTML: true,
  followCursor: true,
  interactive: true,
  theme: 'light', */
})

tippy('.review', {
  arrow: true,
  content: '<b>Review</b><br/>' +
  'This page or section must be reviewed.',
  /* allowHTML: true,
  followCursor: true,
  interactive: true,
  theme: 'light', */
})

tippy('#previousPage', {
  content: "Previous Page",
});

tippy('#nextPage', {
  content: "Next Page",
});

tippy('#createIssue', {
  content: "Create Issue",
});

tippy('#submitPR', {
  content: "Edit this page and submit a pull request",
});

/* Tooltips for terms */
tippy('#sso', {
  content: '<b>SSO – single sign-on</b><br>' +
  'authentication process that allows a user to access multiple applications with one set of login credentials',
  /* trigger: 'click' */
});

tippy('#term', {
  content: "this is a tooltip example",
  /* trigger: 'click' */
});

tippy('#navigatingtip', {
  content: "You can also click the box at the bottom of this menu to view documentation for a specific product and version.",
  /* trigger: 'click' */
});

tippy('#fips', {
  content: '<b>FIPS – Federal Information Processing Standard</b><br>' +
  'FIPS 140-2 or Federal Information Processing Standard Publication 140-2 is a U.S. government computer security standard used to approve cryptographic modules',
  /* trigger: 'click' */
});

tippy('#tpm', {
  content: '<b>TPM – trusted platform module</b><br>' +
  'computer chip that can securely store passwords, certificates, or encryption keys used to authenticate the platform',
  /* trigger: 'click' */
});

tippy('#vsc', {
  content: '<b>VSC – virtual smart card</b><br>' +
  '',
  /* trigger: 'click' */
});

tippy('#ca', {
  content: '<b>CA – certification authority</b><br>' +
  'certifies the ownership of a public key by the named subject of the certificate',
  /* trigger: 'click' */
});

tippy('#crl', {
  content: '<b>CRL – certificate revocation list</b><br>' +
  'list of digital certificates that have been revoked by the issuing certificate authority (CA) before their scheduled expiration date and should no longer be trusted',
  /* trigger: 'click' */
});

tippy('#tooltip', {
  content: '<b>tooltip</b><br>' +
  'used to specify extra information about something when the user moves the mouse pointer over an element',
  /* trigger: 'click' */
});
