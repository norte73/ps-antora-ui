'use strict'

module.exports = (path, lang) => lang + '/' + path.split('/').pop()
