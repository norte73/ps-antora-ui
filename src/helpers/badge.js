'use strict'

const DEPRECATED_RX = / \(deprecated\)/g
const PREVIEW_RX = / \(preview\)/g
const TOBEDELETED_RX = / \(tobedeleted\)/g
const NEW_RX = / \(new\)/g
const REVIEW_RX = / \(review\)/g
module.exports = (html) => {
  if (html) {
    if (typeof html === 'object') {
      html = html.toString()
    }
    html = html.replace(DEPRECATED_RX,
      '<i class="badge deprecated"></i>'
    )
    html = html.replace(PREVIEW_RX,
      '<i class="badge preview"></i>'
    )
    html = html.replace(TOBEDELETED_RX,
      '<i class="badge tobedeleted"></i>'
    )
    html = html.replace(NEW_RX,
      '<i class="badge new"></i>'
    )
    html = html.replace(REVIEW_RX,
      '<i class="badge review"></i>'
    )
  }
  return html
}
